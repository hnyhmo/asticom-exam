$(function() {

    const endpoint = 'http://127.0.0.1:3000/';
    // GET Data On first load
    getData('');
    function getData(src){
        $.ajax({
            url: endpoint,
            data: {src: src},
            type: 'get',
            dataType: 'json',
            success: function(res){
                $html = "";
                for (var i = 0; i < res.length; i++){
                    var obj = res[i];

                    $upload = (obj.file)?"<a href='"+endpoint+"download?file="+obj.file+"'>download</a>":"<form><label class='btn btn2'>upload<input type='file' style='display:none' name='file' class='file'/><input type='hidden' name='id' value='"+obj.key+"' /></label></form>";
                    $html += "<tr>"+
                                "<td>"+obj.docu_no+"</td>"+
                                "<td>"+obj.docu_title+"</td>"+
                                "<td>"+obj.group+"</td>"+
                                "<td>"+obj.by+"</td>"+
                                "<td class='text-center'>"+$upload+"</td>"+
                            +"</tr>";
                }
                if($html){
                    $('tbody').html('');
                    $('tbody').append($html)
                }
                  
            },
            error: function(s){
                alert("Something went wrong")
            }
        });
    }

    // EXPORT
    $('#export').click(function(){
        $.ajax({
            url: endpoint+'google',
            type: 'get',
            dataType: 'json',
            success: function(res){
                if(res.success){
                    window.open(
                        res.url,
                        '_blank'
                      );
                }
            }}
        );
    })


    // Search Here
    $("#src").on('keypress',function(e) {
        if(e.which == 13) {
            var v = $(this).val()
            getData(v)
        }
    });

    
    // Upload
    $('body').on('change', '.file', function(){
        var form = new FormData(jQuery($(this).closest('form'))[0]);
        if($(this).val() != ''){
            $.ajax({
                url: endpoint+'upload',
                type: 'post',
                data: form,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(res){
                    getData()
                },
                error: function(s){
                    alert("Something went wrong")
                }
            });
        }
    })

    $('body').on('click', '#add', function(){
        $form = $(this).closest('form');
        if($("[name='docu_no']").val() != '' && $("[name='docu_title']").val() != '' && $("[name='group']").val() != '' && $("[name='by']").val() != ''){
            $.ajax({
                url: endpoint+'add',
                type: 'post',
                data: $form.serialize(),
                dataType: 'json',
                success: function(res){
                    $form.find('input').val('')
                    getData()
                },
                error: function(s){
                    alert("Something went wrong")
                }
            });
        }
    })





})