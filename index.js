const express = require('express')
var {google} = require('googleapis');

var multer  = require('multer')

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'files/')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname)
	}
})
var upload = multer({storage: storage})


var fs = require("fs"),json;
const app = express()
const port = 3000

const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set("view engine", "ejs");

// GET
app.get('/', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');
	
	let rawdata = fs.readFileSync('data.json');
	let data = JSON.parse(rawdata);

    let src = req.param('src');
	// SEARCH HERE
	if(src){
		let found = [];
		let re = new RegExp(src, 'i');
		data.forEach(function(item, ix) {
			Object.keys(item).forEach(function(key) {
				if (typeof item[key] !== 'string') return;
				if (item[key].match(re)) {
					if (found.indexOf(ix) === -1) { 
					found.push(data[ix]); 
					}
				}
			});
		});
		data = found;
	}
    res.end(JSON.stringify(data));
})

// ADD
app.post('/add', function (req, res) {
	
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');
    
	fs.readFile('data.json', 'utf8', (err, jsonString) => {
		let js = JSON.parse(jsonString);
		let new_data = {
			key:js.length-1,
			docu_no: req.body.docu_no,
			docu_title: req.body.docu_title,
			group: req.body.group,
			by: req.body.by,
			file:""
		}

		js.push(new_data)

		console.log(new_data)

		try {
			fs.writeFileSync('data.json', JSON.stringify(js))
		} catch (err) {
			res.end(JSON.stringify({msg: err, success: true}));
		}
	})

    res.end(JSON.stringify({msg: 'added', success: true}));

	return;
	
})

// UPLOAD 
app.post('/upload', upload.single('file'), function (req, res) {
	
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');
    let id = req.body.id;
	fs.readFile('data.json', 'utf8', (err, jsonString) => {
		let js = JSON.parse(jsonString);
		if(js[id]){
			js[id].file = req.file.filename;
 		}
		 
		try {
			fs.writeFileSync('data.json', JSON.stringify(js))
		} catch (err) {
			res.end(JSON.stringify({msg: err, success: true}));
		}
	})

    res.end(JSON.stringify({msg: 'uploaded', success: true}));

	return;
	
})

// DOWNLOAD
app.get('/download', function(req, res){
	
    let f = req.param('file');
	const file = `${__dirname}/files/${f}`;
	res.download(file);
});

app.get("/google", function(req, res){
	
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');
	
	var creds = require('./google.json');

	// configure a JWT auth client
		let jwtClient = new google.auth.JWT(
			creds.client_email,
			null,
			creds.private_key,
			['https://www.googleapis.com/auth/spreadsheets']);
		//authenticate request
		jwtClient.authorize(function (err, tokens) {
		if (err) {
			console.log(err);
			return;
		} else {
			
			fs.readFile('data.json', 'utf8', (err, jsonString) => {
				let js = JSON.parse(jsonString);
				var result = [];
				for(let v of js) {
					result.push([v.docu_no, v.docu_title, v.group, v.by, v.file])
				}
					

					let spreadsheetId = '1ZfUj7_xjXPPXXlTXf4yOHEGc8XsZ-szvQ47HSC6Kq7c';
					let sheets = google.sheets('v4');
					
					sheets.spreadsheets.batchUpdate({
						auth: jwtClient,
						spreadsheetId: spreadsheetId,
						resource: {
						"requests": 
						[
							{
							"deleteRange": 
							{
								"range": 
								{
								"sheetId": 0,
								},
								"shiftDimension": "ROWS"
							}
							}
						]
						}
					}, (err, response) => {
						if(!err){					
							sheets.spreadsheets.values.append({
								spreadsheetId: spreadsheetId,
								range: 'Sheet1',
								valueInputOption: 'RAW',
								insertDataOption: 'INSERT_ROWS',
								resource: {
									values: result,
								},
								auth: jwtClient,
							}, (err, response) => {
								
							})
						}
						
						res.end(JSON.stringify({msg: 'success', success: true, url: 'https://docs.google.com/spreadsheets/d/1ZfUj7_xjXPPXXlTXf4yOHEGc8XsZ-szvQ47HSC6Kq7c/edit#gid=0'}));
						return;
					});

			})

			
			
		}
		});


})


app.listen(port, () => {
  console.log(`App is running at http://localhost:${port}`)
})